/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo;

import java.io.IOException;
import java.net.DatagramPacket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jideesh
 */
public class UDPThread extends Thread{
    private byte[] buf = new byte[1000];
    public void run()
    {
        DatagramPacket packet =  new DatagramPacket(buf, buf.length);
        while(true)
        {
            try {
                ServerApplication.dsocket.receive(packet);
                System.out.println("UDP Data Arrived");
            } catch (IOException ex) {
                Logger.getLogger(UDPThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
